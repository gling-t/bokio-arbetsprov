

const banks = [
  {
    key: 'hb',
    name: 'Handelsbanken',
    href: 'https://www.handelsbanken.se',
    logo: {
      src: 'handelsbanken-logo.jpg',
      height: 40,
      style: {
        transform: 'translateY(3px)'
      }
    }
  },
  {
    key: 'swedbank',
    name: 'Swedbank',
    href: 'https://www.swedbank.se',
    logo: {
      src: 'logo_swedbank.png',
      height: 40,
      style: {
        transform: 'translateY(4px)'
      }
    }
  },
  {
    key: 'nordea',
    name: 'Nordea',
    href: 'https://www.nordea.se',
    logo: {
      src: 'nordea_logo.png',
      height: 40,
      style: {
        transform: 'translateY(2px)'
      }
    }
  },
  {
    key: 'seb',
    name: 'SEB',
    href: 'https://www.seb.se',
    logo: {
      src: 'seb_logo.jpg',
      height: 40,
      style: {
        transform: 'translateY(3px)'
      }
    },
  }
];


export default banks;