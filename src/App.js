import React, { Component } from 'react';
import './App.css';

import TransactionsPanel from './components/TransactionsPanel';
import PageTemplate from './components/PageTemplate';

class App extends Component {
  render() {
    return (
      <div className="App">
        <PageTemplate>
          <TransactionsPanel />
        </PageTemplate>
      </div>
    );
  }
}

export default App;
