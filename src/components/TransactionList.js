import React from 'react';

export default class TransactionList extends React.Component {

    render() {
      const { result, error, loading } = this.props;
      if (loading) {
        return <div className="loader" style={{ top: 200, left: 280 }} />
      }
      const items = result;
      if (!items || items.length === 0) {
        return (
          <div style={{ padding: 20 }}>
            No transactions here yet!
          </div>
        );
      }
      return (
        <table className="account-entry-list" style={{}}>
          <thead>
            <tr>
              <th width="150">Date</th>
              <th width="150">Description</th>
              <th width="100" style={{ textAlign: 'right' }}>Amount</th>
            </tr>
          </thead>
          <tbody>
          {
            items.map(item =>
              <tr key={item.id}>
                <td>{item.date.substr(0,10)}</td>
                <td>{item.text}</td>
                <td style={{ textAlign: 'right' }}>{item.amount},00</td>
              </tr>
            )
          }
          </tbody>
        </table>
      );
    }

}
