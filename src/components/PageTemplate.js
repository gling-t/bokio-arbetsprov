import React from 'react';

export default class PageTemplate extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <div className="page">
        <div className="page-header">
          <img src="logo-green-small.png" alt="Bokio" width="33" style={{ display: 'inline-block', verticalAlign: 'middle'}} />
          <span className="logo-text" style={{ display: 'inline-block', verticalAlign: 'middle', marginLeft: 4 }}>Bokio</span>
        </div>
        <div className="account-panel">
          {this.props.children}
        </div>
      </div>
    );
  }

}


