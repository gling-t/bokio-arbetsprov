import React from 'react';

export default ({ children }) =>
<div style={{ display: 'flex', flexDirection: 'row', padding: 8 }}>
  <div style={{ flexGrow: 1 }}>
    &nbsp;
  </div>
  {children}
</div>;
