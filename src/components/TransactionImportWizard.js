import React from 'react';
import * as service from '../services/transactions';
import banks from '../configuration/banks';
import ButtonBar from './ButtonBar';

function iterate(n) {
  const arr = [];
  for (let i=0;i<n;i++) {
    arr.push(i+1);
  }
  return arr;
}

const Pager = ({ pageSize, count }) =>
  <div>
    {
      count > pageSize
      ?
        <div style={{ marginTop: 10, display: 'flex', justifyContent: 'center' }}>
          Pages
          {
            iterate(count / pageSize).map(
              n => <a href="#" style={{ margin: '0px 5px' }} onClick={
                () => { alert('Not implemented') }
              }>{n}</a>
            )
          }
        </div>
      : null
    }
  </div>

export default class TransactionImportWizard extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        seletedRadioKey: null,
        selectedBank: null,
        loading: false,
        response: null,
        editResponse: false,
      }
      this.onChoiceButtonChange = this.onChoiceButtonChange.bind(this);
      this.onCommitSelectedBank = this.onCommitSelectedBank.bind(this);
      this.onDeselectBank = this.onDeselectBank.bind(this);
      this.onTextAreaChange = this.onTextAreaChange.bind(this);
      this.onCommitText = this.onCommitText.bind(this);
      this.onResetResponse = this.onResetResponse.bind(this);
      this.onEditResponse = this.onEditResponse.bind(this);
      this.onEditResponseCancel = this.onEditResponseCancel.bind(this);
      this.onEditResponseCommit = this.onEditResponseCommit.bind(this);
      this.onCommitImport = this.onCommitImport.bind(this);
    }

    onCommitImport() {
      const { response } = this.state;
      console.log(response);
      
      const transactions = response.rows.map(
        row => {
          const b = {};
          response.suggestions.forEach(
            col => {
              b[col.selectedOption.key] = row.cells[col.index].cleanedValue;
            }
          );
          return ({
            id: row.index,
            date: `${b.Date}T00:00:00.000Z`,
            text: b.Message,
            amount: parseFloat(b.Sum.replace(/ /g,'')),
          });
        }
      );
      
      this.props.onCommit(transactions);
    }

    onEditResponse() {
      this.setState({ editResponse: true });
    }

    onEditResponseCommit() {
      this.setState({ editResponse: false });
    }

    onEditResponseCancel() {
      this.setState({ editResponse: false });
    }

    onChoiceButtonChange(e) {
      this.setState({ selectedRadioKey: e.target.id});
    }

    onCommitSelectedBank() {
      this.setState({ selectedBank: this.state.selectedRadioKey });
    }

    onDeselectBank() {
      this.setState({ selectedBank: null });
    }

    onTextAreaChange(e) {
      this.setState({ text: e.target.value });
    }

    onCommitText() {
      this.setState({ loading: true });
      service.parseBankPage(this.state.text)
      .then(result => {
        console.log('RESULT', result);
        this.setState({ response: result, loading: false })
      })
      .catch(err => {
        console.error(err);
        this.setState({ loading: false })
      });
    }

    onResetResponse() {
      this.setState({ response: null });
    }

    renderChooseBank() {
      const { selectedBank, selectedRadioKey } = this.state;
      return (
        <div>
          <div style={{ flexGrow: 1, padding: 20 }}>
            <div style={{ lineHeight: '28px' }}>Please select your bank</div>
            {
              banks.map(bank =>
                <div key={bank.key} style={{ display: 'flex', alignItems: 'center' }}>
                  <input
                    type="radio"
                    name="bank"
                    id={bank.key}
                    onChange={this.onChoiceButtonChange}
                    value={bank.key === selectedRadioKey ? 'on' : 'off'}
                  />
                  <label htmlFor={bank.key}>
                    <img {...bank.logo} alt={bank.name} />
                  </label>
                </div>
              )
            }
            <div style={{ height: 0, background: '#ccc', margin: '4px 0' }} />
            <div key="other" style={{ display: 'flex', alignItems: 'center' }}>
              <input
                type="radio"
                name="bank" id="other" onChange={this.onChoiceButtonChange}
                value={'other' === selectedRadioKey ? 'on' : 'off'}
              />
              <label htmlFor={'other'} style={{ lineHeight: '40px', paddingLeft: 5 }}>
                Other
              </label>
            </div>
          </div>
          <ButtonBar>
            <button
              className="action-button"
              disabled={selectedRadioKey===null}
              onClick={this.onCommitSelectedBank}
            >NEXT</button>
          </ButtonBar>
        </div>
        
      );

    }

    renderResponseSummary() {
      const { response } = this.state;
      const items = response.rows.slice(0,10);
      const cols = response.suggestions.filter(cell => cell.selectedOption.label !== 'Ignore');
      return (
        <div>
          <table style={{ width: '100%' }}>
            <thead>
              <tr>
              {cols.map(
                  cell =>
                    <th key={cell.index}>
                      {cell.selectedOption.label}
                    </th>
              )}
              </tr>
            </thead>
            <tbody>
          {items.map(
            row =>
              <tr key={row.index}>
                {
                  cols.map(
                    col =>
                      <td key={col.index}>
                        {row.cells[col.index].cleanedValue}
                      </td>
                  )
                }
              </tr>
          )}
            </tbody>
          </table>
          <Pager pageSize={10} count={response.rows.length} />
        </div>
      );
    }

    renderResponseEditor() {
      const { response } = this.state;
      const items = response.rows.slice(0,10);
      return (
        <div>
          <table>
            <thead>
              <tr>
              {response.suggestions.map(
                  cell =>
                    <th key={cell.index}>
                      <select defaultValue={cell.selectedOption.key}>
                        {cell.options.map(
                          option =>
                            <option value={option.key} key={option.key} selected={option.key === cell.selectedOption.key}>
                              {option.label}
                            </option>
                        )}
                      </select>
                    </th>
              )}
              </tr>
            </thead>
            <tbody>
          {items.map(
            row =>
              <tr key={row.index}>
                {
                  row.cells.map(
                    cell =>
                      <td key={cell.index}>
                        {cell.cleanedValue}
                      </td>
                  )
                }
              </tr>
          )}
            </tbody>
          </table>
          <Pager pageSize={10} count={response.rows.length} />
        </div>
      );
    }

    renderCleanup() {
      const { response, editResponse } = this.state;
      const items = response.rows.slice(0,10);
      if (editResponse) {
        return (
          <div>
            <div style={{ padding: 20}}>
              <div style={{ marginBottom: 10 }}>
                Edit transactions before importing
              </div>
              { this.renderResponseEditor() }
            </div>
            <ButtonBar>
              <button className="plain-button" onClick={this.onEditResponseCancel}>
                CANCEL
              </button>
              <div style={{ margin: 2}} />
              <button className="action-button" onClick={this.onEditResponseCommit}>
                DONE
              </button>
            </ButtonBar>          

          </div>
        );
      }
      return (
        <div>
          <div style={{ padding: 20}}>
            <div style={{ paddingBottom: 10 }}>
              Is this the data you would like to import?
            </div>
            { this.renderResponseSummary() }
          </div>
          <ButtonBar>
            <button className="plain-button" onClick={this.onResetResponse}>
              BACK
            </button>
            <div style={{ margin: 2}} />
            <button key="btnEditResponse" className="edit-button" onClick={this.onEditResponse}>
              EDIT
            </button>
            <div style={{ margin: 2}} />
            <button className="action-button" onClick={this.onCommitImport}>
              IMPORT
            </button>
          </ButtonBar>          

        </div>
      );
    }

    renderImport() {
      const { selectedBank, selectedRadioKey, response } = this.state;
      if (response !== null) {
        return this.renderCleanup();
      }
      const bank = banks.filter(b => b.key === selectedBank)[0];
      if (bank == null) {
        return (
          <div>
            <div style={{ padding: 20 }}>
              INSERT TEXT HERE.
              <br />
              <br />SORRY, WE DONT SUPPORT YOUR BANK YET. 
              <br />Please do this instead: blar blar blar
            </div>
            <ButtonBar>
              <button
                className="plain-button"
                disabled={selectedRadioKey===null}
                onClick={this.onDeselectBank}
              >BACK</button>
            </ButtonBar>
          </div>
        );
      }
      return (
        <div>
          <div style={{ padding: 20 }}>
            <div style={{ paddingBottom: 5, fontWeight: 500 }}>Selected bank</div>
            <div style={{ paddingBottom: 15 }}>
              {bank.name.toUpperCase()}
            </div>
            <div style={{ paddingBottom: 5, fontWeight: 500 }}>Import instructions</div>
            <div style={{ paddingBottom: 5 }}>
              <ul>
                <li>
              Login at <a target="_blank" href={bank.href}>{bank.name}</a>.
                </li>
                <li>
                  Navigate to your bank account. 
                </li>
                <li>
                  Click on any empty space in the page (do not click a link, for example).
                </li>
                <li>Press <span className="keypress">Ctrl</span>+<span className="keypress">A</span> followed
              by <span className="keypress">Ctrl</span>+<span className="keypress">C</span> to select all data.
                </li>
                <li>
                  Return here and paste the selected data into the box below:
                </li>
              </ul>
            </div>
            <div>
              <textarea style={{ width: '100%', height: 200, resize: 'none' }} onChange={this.onTextAreaChange}>{this.state.text}</textarea>
            </div>
          </div>
          <ButtonBar>
            <button
              className="plain-button"
              onClick={this.onDeselectBank}
            >BACK</button>
            <div style={{ margin: 2}} />
            <button
              className="action-button"
              onClick={this.onCommitText}
            >NEXT</button>
        </ButtonBar>
      </div>
      );
    }

    render() {
      const { selectedBank, selectedRadioKey , loading} = this.state;

      return (
        <div className="account-entry-import-wizard" style={{ flexGrow: 1, display: 'flex'}}>
          <div style={{ flexGrow: 1, display: 'flex', flexDirection: 'column' }}>
            <div style={{ display: 'flex', padding: 20, backgroundColor: '#28cdaa', color: 'white', fill: 'white', borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
              <div style={{ flexGrow: 1, fontSize: 22 }}>
                Import transactions
              </div>
              <button
                onClick={this.props.onCancel}
                style={{ background: 'none', border: 'none', padding: 0, cursor: 'pointer' }}
              >
                <img src="close.svg" width="20" height="20" />
              </button>
            </div>
            {
              selectedBank === null 
              ? 
                this.renderChooseBank()
              : 
                this.renderImport()
            }
          </div>
          {
            loading
            ?
              <div className="loader" style={{ top: 140, left: 230 }} />
            :
              null  
          }
        </div>
      );
    }

}
