import React from 'react';

const isParentChild = (parent, child) => {
  if (parent == null || child == null) {
    return false;
  }
  if (parent === child) {
    return true;
  }
  return isParentChild(parent, child.parentNode);
}

export default class Dialog extends React.Component {

  constructor(props) {
    super(props);
    this.state = { loading: true, items: [], showImportDialog: false };
    this.onDocumentMouseDown = this.onDocumentMouseDown.bind(this);
    this.setDialogElement = this.setDialogElement.bind(this);
  }

  onDocumentMouseDown(e) {
    if (isParentChild(this.dialogElement, e.target)) {
      // mousedown inside dialog => do nothing
    } else {
      // this.props.onClose();
    }
  }

  setDialogElement(element) {
    this.dialogElement = element;
  }

  componentDidMount() {
    document.addEventListener("mousedown", this.onDocumentMouseDown);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.onDocumentMouseDown);
  }

  render() {
    return (
      <div className="lightbox">
        <div className="dialog" style={{ display: 'flex', position: 'relative' }} ref={this.setDialogElement}>
          {this.props.children}
        </div>
      </div>
    );
  }

}


