import React from 'react';
import TransactionImportWizard from './TransactionImportWizard';
import TransactionList from './TransactionList';
import Dialog from './Dialog';
import * as service from '../services/transactions';

export default class TransactionsPanel extends React.Component {

  constructor(props) {
    super(props);
    this.state = { loading: false, items: [], showImportDialog: false };
    this.onImportDialogClose = this.onImportDialogClose.bind(this);
    this.onImportDialogCommit = this.onImportDialogCommit.bind(this);
  }

  componentDidMount() {
    this.loadTransactions();
  }

  loadTransactions() {
    this.setState({ loading: true });
    service.getTransactions()
    .then(result => {
      this.setState({ loading: false, result, error: null });
    })
    .catch(error => {
      this.setState({ loading: false, result: null, error });
    });
  }

  onImportDialogCommit(transactions) {
    this.setState({ showImportDialog: false, importing: true });
    service.addTransactions(transactions)
    .then(() => {
      this.setState({ importing: false });
      this.loadTransactions();
    })
    .catch(() => {
      this.setState({ importing: false });
      this.loadTransactions();
    });
  }

  onImportDialogClose() {
    this.setState({ showImportDialog: false });
  }

  onImportButtonClick() {
    this.setState({ showImportDialog: true});
  }

  render() {
    const { loading, importing, items, showImportDialog } = this.state;

    return (
      <div>
        <div className="account-panel" style={{ position: 'relative' }}>
          <div className="account-panel-title" style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <div style={{ flexGrow: 1 }}>
            Transactions
            </div>
            <button
              className="action-button"
              onClick={() => this.onImportButtonClick()}
            >Import from your bank</button>
          </div>
          <TransactionList {...this.state} />
          {
            importing
            ?
              <div className="loader" style={{}} />
            :
              null
          }
        </div>
          {
            showImportDialog
            ?
              <Dialog onClose={this.onImportDialogClose}>
                <TransactionImportWizard
                  onCancel={this.onImportDialogClose}
                  onCommit={this.onImportDialogCommit}
                />
              </Dialog>
            :
              null
          }
      </div>
    );
  }

}


