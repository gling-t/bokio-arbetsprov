import $ from 'jquery';
const baseUrl = 'http://bokiotestbankapi.azurewebsites.net/api'

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}


const key = getParameterByName("key");


function call(path, type, data) {
  console.log('SENDING DATA', data);
  return $.ajax({
    url: `${baseUrl}/${key}${path}`,
    type,
    contentType: "application/json; charset=utf-8",
    dataType: 'json',
    data: JSON.stringify(data),
  });
}

export function parseBankPage(text) {
  return call('/Transactions/Parse', 'POST', { text });
}

export function getTransactions() {
  return call('/Transactions', 'GET', {});
}

export function addTransactions(transactions) {
  return call('/Transactions', 'POST', transactions);
}